import axios from 'axios';

const API_KEY='2d22b36f578bd8f611f15b538fa6f88b';
//const API_KEY='AIzaSyBj3Go57D3WOCRmgh4SGneulqoH05lx1AI';
const ROOT_URL=`http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=${API_KEY}`;

export const FETCH_WEATHER='FETCH_WEATHER';

export function fetchWeather(city){
    return dispatch =>{
        const url =`${ROOT_URL}&q=${city}&unit=metric`;

        axios.get(url)
            .then(response=>{
                dispatch({
                    type:FETCH_WEATHER,
                    payload:response
                });
            });
    }
}




// Previous function without redux-thunk
// export function fetchWeather(city){     
//     const url =`${ROOT_URL}&q=${city}&units=metric`;
//     const request = axios.get(url);

//     return{
//         type:FETCH_WEATHER,
//         payload:request
//     }
// }