import React from 'react';
import ReactDOM from 'react-dom';


import {Provider} from 'react-redux';
import{createStore,applyMiddleware}from 'redux';
//import ReduxPromise from 'redux-promise';
import ReduxThunk from 'redux-thunk';
import Log from './middleware/log';

import './index.css';
import App from './components/app';

import reducers from './reducers';

import * as serviceWorker from './serviceWorker';

const createStoreWithMiddleWare = applyMiddleware(Log,ReduxThunk)(createStore);



ReactDOM.render(
    <Provider store ={createStoreWithMiddleWare(reducers)}>
         
         <App />
    </Provider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
