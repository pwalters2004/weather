/*global google*/
import React, { Component } from 'react';
import Map from 'google-maps-react';
export default class TheGoogleMaps extends Component {
   
    componentDidMount() {
        new google.maps.Map(this.refs.map, {
            zoom: 15,
            center: {
                lat: this.props.lat,
                lng: this.props.lon
            }
        });
    }
 render(){
   return <div ref ='map'/>;
 }
// onClick={this.onMapClicked}

    // render() {
    //     return  (<Map
    //     google={this.props.google}
    //     style={{height: 500, width: 500}}
    //     center={{
    //       lat: 40.854885,
    //       lng: -88.081807
    //     }}
    //     zoom={15}
       
    //   />)
    // }
}
