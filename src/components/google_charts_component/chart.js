import _ from 'lodash';
import React from 'react';
import {Sparklines,SparklinesLine,SparklinesReferenceLine}from 'react-sparklines';

function average(data){
    
    return _.round(_.sum(data)/data.length);
}

export default (props) =>{
       
    return(
        <div>
            <Sparklines height={props.height} width={props.width} data={props.data}>
            <SparklinesReferenceLine type="min" />
                <SparklinesLine color={props.color}  style={{ fill: "#41c349", fillOpacity:".25" }}/>
            <SparklinesReferenceLine type="median"/>
            </Sparklines>
            <div>
                {average(props.data)}
                {props.unit}
            </div>
        </div>
    );
}