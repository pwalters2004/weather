import React from 'react';
import Chart from './chart';
import {shallow} from 'enzyme';
import "../../test/setUpTest";

describe('<Chart/>',()=>{

const props = {
   data: [296.115,293.45],
   height: 120,
   width: 200,
   color: 'red',
   unit:'Celsius'
}

    it('renders without crashing',()=>{
        shallow(<Chart data={props.data} />)
    });

});