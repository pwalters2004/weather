# [Simple Weather App](#)


[Simple Weather App](#) is a simple app to explore React, Redux, Redux-Thunk.

## Preview

## Status

[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/BlackrockDigital/startbootstrap-agency/master/LICENSE)
[![npm version](https://img.shields.io/npm/v/startbootstrap-agency.svg)](https://www.npmjs.com/package/startbootstrap-agency)
[![CircleCI - Build Status](https://circleci.com/gh/pwalters04/Japan-/tree/master.svg?style=svg)](https://circleci.com/gh/pwalters04/Japan-/tree/master)
[![dependencies Status](https://david-dm.org/BlackrockDigital/startbootstrap-agency/status.svg)](https://david-dm.org/BlackrockDigital/startbootstrap-agency)
[![devDependencies Status](https://david-dm.org/BlackrockDigital/startbootstrap-agency/dev-status.svg)](https://david-dm.org/BlackrockDigital/startbootstrap-agency?type=dev)



## Run Simple Weather App - Basics
* Clone the repo: `git clone`
*  Install via npm: `npm i `
*  Start App: `Yarn Start`

